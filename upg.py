import subprocess
import re
import os
import sys
from datetime import datetime


def clr():
    os.system('clear')

def ERR(msg):
    print("\033[1;31m[-] ERROR: {}\033[0m".format(msg))
    sys.exit(86)

def check_root():
    if os.geteuid() != 0:
        ERR("you must be root")
    else:
        msg('script has root privileges')


def warn(msg):
    print("\033[1;33m[!] WARNING: {}\033[0m".format(msg))
    print()


def msg(msg):
    print("\033[1;32m[+] {}\033[0m".format(msg))

def msg2(msg):
    print("\033[1;36m{}\033[0m".format(msg))

def msg3(msg):
    print("\033[1;32m{}\033[0m".format(msg))

def check_internet():
    try:
        subprocess.check_output(['curl', '-s', '--connect-timeout', '8', 'https://microsoft.com/'])
        msg('Internet is active!')
    except subprocess.CalledProcessError:
        warn("You do NOT have an Internet connection!")


def check_curl():
    try:
        subprocess.check_output(['curl', '--version'])
        msg("Curl is installed")
    except FileNotFoundError:
        subprocess.call(['sudo', 'apt', 'install', '-y', 'curl'])


def UBU_upg():
    msg3('perform full system upgrade? (apt upgrade) [Y/n]:')
    conf = input().lower()
    if conf in ['', 'y', 'yes', 'Y', 'YES', 'Yes', 'yES']:
        clr()
        subprocess.call(['sudo', 'apt', 'upgrade', '-y']) # apt upgrade -y
  #  elif conf in ['n', 'no']:
    else:
        warn('You have chosen NOT to up-to-date your system.')

clr()
# check_root()
check_curl()
check_internet()
# List upgradable packages
list_command = subprocess.run(['sudo', 'apt', 'list', '--upgradable'], capture_output=True, text=True)
upgradable_packages = list_command.stdout

# Define the regular expression pattern
pattern = re.compile(r'^(.*?)/', re.MULTILINE)

# Colorize the package names
colorized_packages = pattern.sub(lambda m: f"\033[92m{m.group(1)}\033[0m/", upgradable_packages)


# Run sudo apt update -y
update_command = subprocess.run(['sudo', 'apt', 'update', '-y'])

# Check the return code of the command
if update_command.returncode == 0:
    clr()
    msg2("Update successful")
    print()
else:
    warn("Update failed")
    print()


# Count the number of upgradable packages
count_command = subprocess.run(['apt', 'list', '--upgradable'], capture_output=True, text=True)
package_count = len(count_command.stdout.strip().split('\n')) - 1

print("Upgradable Packages:")
print(colorized_packages)
# print("Number of Upgradable Packages:", package_count)
print("Number of Upgradable Packages:\033[38;5;208m", package_count, "\033[0m")
UBU_upg()

"""
# Execute the command to get the upgradable packages
command = ['apt', 'list', '--upgradable']
result = subprocess.run(command, capture_output=True, text=True)

# Check if the command was successful
if result.returncode == 0:
    # Split the output by lines and exclude the header line
    package_lines = result.stdout.strip().split('\n')[1:]
    
    # Get the package count
    package_count = len(package_lines)
    
    # Print the upgradable packages
    print("Upgradable Packages:")
    for line in package_lines:
        print(line)
    
    # Print the package count in orange
    print("Number of Upgradable Packages:\033[38;5;208m", package_count, "\033[0m")
else:
    # Print an error message if the command failed
    print("Failed to retrieve upgradable packages. Error:", result.stderr)
"""
