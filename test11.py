import configparser
import subprocess
import os
import sys
from datetime import datetime


def clr():
    os.system('clear')


def ERR(msg):
    print("\033[1;31m[-] ERROR: {}\033[0m".format(msg))
    sys.exit(86)


def check_root():
    if os.geteuid() != 0:
        ERR("you must be root")
    else:
        msg('script has root privileges')


def warn(msg):
    print("\033[1;33m[!] WARNING: {}\033[0m".format(msg))
    print()


def msg(msg):
    print("\033[1;32m[+] {}\033[0m".format(msg))


def msg2(msg):
    print("\033[1;36m    {}\033[0m".format(msg))


def msg3(msg):
    print("\033[1;36m{}\033[0m".format(msg))


def check_internet():
    try:
        subprocess.check_output(['curl', '-s', '--connect-timeout', '8', 'https://microsoft.com/'])
        msg('Internet is active!')
    except subprocess.CalledProcessError:
        warn("You do NOT have an Internet connection!")


def check_curl():
    try:
        subprocess.check_output(['curl', '--version'])
        msg("Curl is installed")
    except FileNotFoundError:
        subprocess.call(['apt', 'install', '-y', 'curl'])


def apt_upd():
    if subprocess.call(['apt', 'update', '-y']) == 0:
        clr()
        subprocess.call(['apt', 'list', '--upgradable'])
        return True
    else:
        warn("Synchronizing apt has failed. Please try manually: apt update -y")
        return False


def apt_upg():
    msg('perform full system upgrade? (apt upgrade -y) [Yn]:')
    conf = input().lower()
    if conf in ['', 'y', 'yes']:
        subprocess.call(['apt', 'upgrade', '-y'])
    elif conf in ['n', 'no']:
        warn('You have chosen NOT to up-to-date your system.')


def lst_D_upd():
    subprocess.call(['apt', 'list', '--upgradable'])


def see_ubu():
    subprocess.call(['apt-get', 'update', '-y'])
    clr()
    pkgNumber = int(subprocess.check_output(['apt list --upgradable | wc -l']))
    pkgNumber -= 1
    clr()
    if pkgNumber == 0:
        msg("Your System Is Up To Date")
        msg2("Nothing To Do....")
        print()
    else:
        pkgStr = "packages"
        if pkgNumber == 1:
            pkgStr = "package"
        subprocess.call(['apt', 'list', '--upgradable'])
        print()
        msg("{} {}".format(pkgNumber, pkgStr))
        print()
        apt_upg()


def see_arch():
    clr()
    subprocess.call(['pacman', '-Sy'])
    pkgNumber = int(subprocess.check_output(['pacman', '-Qu', '|', 'wc', '-l']))
    clr()
    if pkgNumber == 0:
        print("System up to date")
    else:
        pkgStr = "package"
        if pkgNumber > 1:
            pkgStr += "s"
        subprocess.call(['apt', 'list', '--upgradable'])
        print()
        msg("{} {}".format(pkgNumber, pkgStr))
        print()


def pac_upg():
    print('perform full system upgrade? (pacman -Su) [Yn]:')
    conf = input().lower()
    if configparser ['', 'y', 'yes']:
        subprocess.call(['pacman', '-Su', '--noconfirm', '--color', 'always'])
    elif conf in ['n', 'no']:
        warn('You have chosen NOT to up-to-date your system.')


def solus_upd():
    subprocess.call(['sudo', 'eopkg', 'up'])


def solus_upg():
    print('perform full system upgrade? (eopkg upgrade) [Yn]:')
    conf = input().lower()
    if conf in ['', 'y', 'yes']:
        subprocess.call(['sudo', 'eopkg', 'update-repo'])
        subprocess.call(['sudo', 'eopkg', 'upgrade'])
    elif conf in ['n', 'no']:
        warn('You have chosen NOT to up-to-date your system.')


def main():
    today = datetime.now().strftime("%Y-%m-%d")
   # usr1 = os.getlogin()

    clr()
    print()
    print("\033[1;36mStarted  \033[1;93m {}\033[0m".format(today))
    print()

    os2 = subprocess.check_output(['lsb_release', '-si']).decode().strip()
    os = subprocess.check_output(['lsb_release', '-sirc']).decode().strip()
    os1 = os2.lower()

    if os1 != 'ubuntu' and os1 != 'peppermint' and os1 != 'linuxmint' and os1 != 'debian' and os1 != 'kali' and os1 != 'raspian':
        warn("You are using {} but this script is not setup for {}".format(os1.upper(), os1.upper()))
        ERR("Can NOT continue! Exiting.....")

    check_root()
    check_internet()
    check_curl()

    if os1 == 'arch' or os1 == 'manjaro':
        see_arch()
        pac_upg()
    elif os1 == 'ubuntu' or os1 == 'peppermint' or os1 == 'linuxmint' or os1 == 'debian' or os1 == 'kali' or os1 == 'raspian':
        see_ubu()
    elif os1 == 'solus':
        solus_upd()
        solus_upg()


if __name__ == "__main__":
    main()




