import subprocess
import os
import sys
from datetime import datetime

def clr():
    os.system('clear')

def error(message):
    sys.stderr.write(f"[-] ERROR: {message}\n")
    sys.exit(86)

def warn(message):
    sys.stderr.write(f"[!] WARNING: {message}\n")

def msg(message):
    print(f"[+] {message}")

def msg2(message):
    print(f"    {message}")

def msg3(message):
    print(message)

def check_internet():
    try:
        subprocess.check_output(['curl', '-s', '--connect-timeout', '8', 'https://microsoft.com/'])
        msg('Internet is active!')
    except subprocess.CalledProcessError:
        warn('You do NOT have an Internet connection!')

def check_curl():
    try:
        subprocess.check_output(['curl', '--version'])
        msg("Curl is installed")
    except subprocess.CalledProcessError:
        subprocess.call(['apt', 'install', '-y', 'curl'])

def apt_upd():
    if subprocess.call(['apt', 'update', '-y']) == 0:
        clr()
        subprocess.call(['apt', 'list', '--upgradable'])
        return True
    else:
        warn("Synchronizing apt has failed. Please try manually: apt update -y")
        return False

def apt_upg():
    conf = input('Perform full system upgrade? (apt upgrade -y) [Y/n]: ')
    if conf.lower() in ['', 'y', 'yes']:
        msg('You entered yes. This will upgrade your system')
        subprocess.call(['apt', 'upgrade', '-y'])
    elif conf.lower() in ['n', 'no']:
        warn('You have chosen NOT to up-to-date your system.')

def lst_D_upd():
    subprocess.call(['apt', 'list', '--upgradable'])

def see_ubu():
    subprocess.call(['sudo', 'apt-get', 'update', '-y'])
    clr()
    pkgNumber = int(subprocess.check_output(['apt', 'list', '--upgradable', '|', 'wc', '-l']))
    pkgNumber -= 1
    clr()
    if pkgNumber == 0:
        msg("Your System Is Up To Date")
        msg2("Nothing To Do....")
        print("")
    else:
        pkgStr = "packages"
        if pkgNumber == 1:
            pkgStr = "package"
        subprocess.call(['apt', 'list', '--upgradable'])
        print("")
        msg(f"{pkgNumber} {pkgStr}")
        print("")
        apt_upg()

def see_arch():
    clr()
    subprocess.call(['sudo', 'pacman', '-Sy'])
    clr()
    pkgNumber = int(subprocess.check_output(['sudo', 'pacman', '-Qu', '|', 'wc', '-l']))
    if pkgNumber == 0:
        print("System up to date")
    else:
        pkgStr = "package"
        if pkgNumber > 1:
            pkgStr += "s"
        subprocess.call(['apt', 'list', '--upgradable'])
        print("")
        msg(f"{pkgNumber} {pkgStr}")
        print("")

def close_terminal():
    conf = input('Do you want to close the terminal? [Y/n]: ')
    if conf.lower() in ['', 'y', 'yes']:
        msg('Goodbye!')
        sys.exit()
    elif conf.lower() in ['n', 'no']:
        warn('You have chosen NOT to close the terminal.')

def if_then_elif():
    os_info = subprocess.check_output(['lsb_release', '-sirc']).decode().strip().
