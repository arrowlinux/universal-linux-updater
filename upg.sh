#!/usr/bin/env bash
# vim: noai:ts=4:sw=4:expandtab
# shellcheck source=/dev/null
# shellcheck disable=2009
#
# START OF SCRIPT
#
#usrNM="${NUSRN1,,}"

TODAY="$(date)"
USR1="$(logname)"

clear ; clear
echo ""
echo -e "\e[36mStarted  \e[93m $TODAY \e[0m"
#echo ""


OS2="$(lsb_release -si)"
OS="$(lsb_release -sirc)"
OS1="${OS2,,}"

clr(){
  clear ; clear
}

# Error message wrapper function
ERR()
{

	echo ""
	echo >&2 "$(tput bold; tput setaf 1)[-] ERROR: ${*}$(tput sgr0)"
	echo ""
	exit 86
}

# check for root privilege function
check_root()
{
  if [ "$(id -u)" -ne 0 ]; then
    ERR "you must be root"
  else
     msg 'script has root privileges'
  fi
}

# warning message wrapper function
warn()
{
  echo >&2 "$(tput bold; tput setaf 3)[!] WARNING: ${*}$(tput sgr0)"
  echo ""
}

nMbR5()
{
  echo >&2 "$(tput bold; tput setaf 3):  ${*}$(tput sgr0)"
}

# standard message wrapper function
msg()
{
  echo "$(tput bold; tput setaf 2)[+] ${*}$(tput sgr0)"
}

# standard message wrapper function
msg2()
{
  echo "$(tput bold; tput setaf 6)    ${*}$(tput sgr0)"
}

msg3()
{
  echo "$(tput bold; tput setaf 6)${*}$(tput sgr0)"
}

# check for internet connection function
check_internet()
{
  tool='curl'
  tool_opts='-s --connect-timeout 8'

  if ! $tool $tool_opts https://microsoft.com/ > /dev/null 2>&1
    then
        warn "You do NOT have an Internet connection!"

    else 
        msg 'Internet is active!'
  fi

  return $SUCCESS
}


OS_not_in_script(){
  warn "You are using ${OS1^} but this script is not setup for ${OS1^}"
  ERR "Can NOT continue! Exiting....."
}
# check if curl is installed function
check_curl(){
      if [ -f /usr/bin/curl ]
           then 
               msg "Curl is installed"

           else 
               apt install -y curl  

      fi
}

# synchronize and update function
apt_upd()
{
  if apt update -y ; then
      echo ""
      clr
      apt list --upgradable
      echo ""
     return $SUCCESS
  fi

  warn "Synchronizing apt has failed. Please try manually: apt update -y"

  return $FAILURE
}

# Apt update function
apt_upg()
{
  msg 'perform full system upgrade? (apt upgrade -y) [Yn]:'
  read conf < /dev/tty
  case "$conf" in
#    ''|y|Y) msg 'you entered yes. this would have upgraded your system' ;;
    ''|y|Y|yes|YES|Yes|yES|yeS|YEs|YeS|yEs) msg 'you entered yes. this will upgraded your system' ; apt upgrade -y ;;
    *) warn 'You have chosen NOT to up-to-date your system.' ;;
  esac
}

# Debian based list updates function
lst_D_upd(){
   

   apt list --upgradable
}
see_ubu(){
  sudo apt-get update -y
clr
 #i=$((i+1))
  pkgNumber=$(apt list --upgradable | wc -l)

  pkgNumber=$((pkgNumber-1))

  clr
if [ $pkgNumber -eq 0 ]; then
  msg "Your System Is Up To Date"
  msg2 "Nothing To Do...."
  echo ""
else
  pkgStr="packages"
  if [ $pkgNumber == 1 ]; then
 #   pkgStr+="s"
    pkgStr="package"
  fi
  apt list --upgradable
  echo ""
  echo ""

  # msg3 "$pkgStr" ; nMbR5 "$pkgNumber" 
  echo -e "\e[36mThere's \e[93m$pkgNumber \e[36m$pkgStr\e[0m"
  echo ""
  apt_upg
fi
}

see_arch(){

  clr
  pkgNumber=$(sudo pacman -Qu | wc -l)
  sudo pacman -Sy
 
  clr
  if [ $pkgNumber -eq 0 ]; then
  echo "System up to date"
  else
  pkgStr="package"
  if [ $pkgNumber -gt 1 ]; then
    pkgStr+="s"
  fi
  apt list --upgradable
  echo ""
  msg "$pkgNumber $pkgStr"
  echo ""
  fi
}

close_terminal()
{
  warn 'Do you want to close the terminal? [Yn]:'
  read conf < /dev/tty
  case "$conf" in
    ''|y|Y) msg 'GoodBye! ,,,,,,' ; sleep 2 ; kill -9 $PPID ;;
    n|N) warn 'You have chosen NOT to close the terminal.' ;;
  esac
}

###############################################
################# Arch Based ##################
###############################################

# install Pacaur for Arch based system function
Install_Pacaur_Arch(){
          pacman -S git --noconfirm
          cd /home/$USR1/
          sudo -u $USR1 git clone https://aur.archlinux.org/auracle-git.git
          cd /home/$USR1/auracle-git/ ; sudo -u $USR1 makepkg -si --noconfirm
          cd ..
          rm -R /home/$USR1/auracle-git
          sudo -u $USR1 git clone https://aur.archlinux.org/pacaur.git
          cd /home/$USR1/pacaur/
          sudo -u $USR1 makepkg -si --noconfirm
          cd ..
          rm -R /home/$USR1/pacaur
             
         echo ""
         echo "Pacaur is installed."
         echo ""
         echo "Press a key to continue"
         read -rsn1
         echo "Key pressed" ; sleep 1 

}

# pacman update function
pac_upd()
{
  if pacman -Sy; then
    return $SUCCESS
  fi
    ,.,i
  warn "Synchronizing pacman has failed. Please manually try: pacman -Sy"

  return $FAILURE
}

# list Arch updates function
lst_A_upd(){

  if [ -f /usr/bin/pacaur ]
     then
         see_arch
      #   sudo -u $USR1 pacaur -Sy ; clear ; clear ; OUTP2="$(sudo -u $USR1 pacaur -Qu | wc -l)" ; sudo -u $USR1 pacaur -Qu --color always ; echo "" ; echo -e "\e[36mAvailable Updates:\e[93m ${OUTP2}\e[0m"
     else
         warn "Pacaur is not installed. However it is needed in this script"
         msg "Do you want to install Pacaur now?  [Yn]:"
          read Paca < /dev/tty
          case "$Paca" in
#           ''|y|Y) msg 'you entered yes. this would have upgraded your system'  ;;
            ''|y|Y) msg 'you entered yes. We will now install Pacaur.' ; Install_Pacaur_Arch  ;;
               *) warn 'You have chosen NOT to install Pacaur.' 
                    ERR 'Script can not continue! Exiting......' 
                    ;;
          esac
fi
}

# pacman install updates function
pac_upg()
{
  echo 'perform full system upgrade? (pacman -Su) [Yn]:'
  read conf < /dev/tty
  case "$conf" in
#    ''|y|Y) msg 'you entered yes. this would have upgraded your system' ;;
    ''|y|Y) msg 'you entered yes. this will upgraded your system' ; pacman -Su --noconfirm --color always;;
    *) warn 'You have chosen NOT to up-to-date your system.' ;;
  esac
}
solus_upd(){
 
  sudo eopkg up

}

solus_upg(){
    echo 'perform full system upgrade? (eopkg upgrade) [Yn]:'
  read conf < /dev/tty
  case "$conf" in
#    ''|y|Y) msg 'you entered yes. this would have upgraded your system' ;;
    ''|y|Y) msg 'you entered yes. this will upgraded your system' ; sudo eopkg update-repo && sudo eopkg upgrade ;;
    *) warn 'You have chosen NOT to up-to-date your system.' ;;
  esac


}
# run the correct update for the system installed function
if_then_elif(){

# Arch based
if [[ "${OS1}" == 'arch' ]] || [[ "${OS1}" == 'manjaro' ]]
      then
      #  pac_upd
      #  lst_A_upd
        see_arch
        pac_upg

# Debian based
elif [[ "${OS1}" == 'ubuntu' ]] || [[ "${OS1}" == 'peppermint' ]] || [[ "${OS1}" == 'linuxmint' ]] || [[ "${OS1}" == 'debian' ]] || [[ "${OS1}" == 'kali' ]] || [[ "${OS1}" == 'raspian' ]] #|| [[ "${OS1}" == 'raspian' ]]
     then
          see_ubu
    #      apt_upd
          #lst_D_upd
    #      apt_upg




# Solus
elif [[ "${OS1}" == 'solus' ]]
     then
         solus_upd
         solus_upg
     else
          OS_not_in_script
fi
}

list_os(){
    
     echo ""
     msg2 "     You are running"
     msg2 "    "${OS}
     echo ""
}


## RUN UPDATE ##

#echo ""
TIMEFORMAT='Time taken to complete this task, %E Seconds ...'
time {

list_os
check_root
check_curl
check_internet
echo ""
#os_check
echo ""
if_then_elif
echo ""
}
echo ""
TODAY2="$(date)"
pkgStr=" \e[36mpackages"
TheIntro='There were \e[93m'
if [ $pkgNumber == 1 ]; then
   #pkgStr+="s"
   pkgStr=" \e[36mpackage"
   TheIntro='There was \e[93m'
fi
# Gives start and finnish time. For this to work
# the TODAY variable is needed at the top of the script.

#echo -e "\e[36m$TheIntro \e[93m$pkgNumber \e[36m$pkgStr updated"
echo -e "\e[36m$pkgStr updated \e[93m$pkgNumber"
echo ""
echo -e "\e[36mStarted   \e[93m$TODAY \e[36mBy \e[94m${USR1^} \e[0m" 
echo -e "\e[36mFinnished \e[93m$TODAY2\e[0m" 
echo ""
#close_terminal
# End of Script
#
