import os
import subprocess
import sys
from datetime import datetime

def clr():
    os.system('clear')

def err(message):
    print('\n[-] ERROR:', message)
    sys.exit(86)

def warn(message):
    print('[!] WARNING:', message)

def msg(message):
    print('[+]', message)

def msg2(message):
    print('   ', message)

def msg3(message):
    print(message)

def check_root():
    if os.geteuid() != 0:
        err('you must be root')
    else:
        msg('script has root privileges')

def check_internet():
    try:
        subprocess.check_output(['curl', '-s', '--connect-timeout', '8', 'https://microsoft.com/'])
        msg('Internet is active!')
    except subprocess.CalledProcessError:
        warn('You do NOT have an Internet connection!')

def check_curl():
    if not os.path.exists('/usr/bin/curl'):
        subprocess.run(['apt', 'install', '-y', 'curl'])

def apt_upd():
    if subprocess.run(['apt', 'update', '-y']).returncode == 0:
        clr()
        subprocess.run(['apt', 'list', '--upgradable'])
        return True
    else:
        warn('Synchronizing apt has failed. Please try manually: apt update -y')
        return False

def apt_upg():
    choice = input('perform full system upgrade? (apt upgrade -y) [Yn]: ')
    if choice.lower() in ['', 'y', 'yes']:
        subprocess.run(['apt', 'upgrade', '-y'])
    else:
        warn('You have chosen NOT to up-to-date your system.')

def see_ubu():
    subprocess.run(['sudo', 'apt-get', 'update', '-y'])
    clr()
    pkg_list = subprocess.check_output(['apt', 'list', '--upgradable']).decode()
    pkg_number = pkg_list.count('\n') - 1
    if pkg_number == 0:
        msg('Your System Is Up To Date')
        msg2('Nothing To Do....')
        print()
    else:
        print(pkg_list)
        print()
        msg(f'{pkg_number} packages')
        print()
        apt_upg()

def see_arch():
    clr()
    subprocess.run(['sudo', 'pacman', '-Sy'])
    pkg_list = subprocess.check_output(['sudo', 'pacman', '-Qu']).decode()
    pkg_number = pkg_list.count('\n')
    clr()
    if pkg_number == 0:
        print('System up to date')
    else:
        pkg_str = 'packages' if pkg_number > 1 else 'package'
        print(pkg_list)
        print()
        msg(f'{pkg_number} {pkg_str}')
        print()

def close_terminal():
    choice = input('Do you want to close the terminal? [Yn]: ')
    if choice.lower() in ['', 'y', 'yes']:
        msg('GoodBye! ,,,,,,')
        time.sleep(2)
        os.kill(os.getpid(), 9)
    else:
        warn('You have chosen NOT to close the terminal.')

def arch_based_update():
    see_arch()

def debian_based_update():
    see_ubu()

def solus_update():
    subprocess.run(['sudo', 'eopkg', 'up'])

def solus_upgrade():
    choice = input('perform full system upgrade? (eopkg upgrade) [Yn]: ')
    if choice.lower() in ['', 'y', 'yes']:
        subprocess.run(['sudo', 'eopkg', 'update-repo'])
        subprocess.run(['sudo', 'eopkg', 'upgrade'])
    else:
        warn('You have chosen NOT to up-to-date your system.')

def list_os():
    os_name = subprocess.check_output(['lsb_release', '-sirc']).decode().strip()
    print()
    msg2('     You are running')
    msg2(os_name)
    print()

def main():
    clr()
    list_os()
    check_root()
    check_curl()
    check_internet()
    print()
    if 'ubuntu' in os_name.lower() or 'peppermint' in os_name.lower() or 'linuxmint' in os_name.lower() or 'debian' in os_name.lower() or 'kali' in os_name.lower() or 'raspian' in os_name.lower():
        debian_based_update()
    elif 'arch' in os_name.lower() or 'manjaro' in os_name.lower():
        arch_based_update()
    elif 'solus' in os_name.lower():
        solus_update()
        solus_upgrade()
    else:
        warn(f'You are using {os_name} but this script is not set up for {os_name}')
        err('Can NOT continue! Exiting.....')

if __name__ == '__main__':
    today = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    today2 = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    usr1 = os.getlogin()

    clr()
    print()
    print('\033[36mStarted  \033[93m', today, '\033[0m')
    print()

    main()

    print()
    pkg_str = 'packages'
    if pkg_number == 1:
        pkg_str = 'package'
    print('\033[36m', pkg_str, 'updated \033[93m', pkg_number)
    print()
    print('\033[36mStarted   \033[93m', today, '\033[36mBy \033[94m', usr1.upper(), '\033[0m')
    print('\033[36mFinnished \033[93m', today2, '\033[0m')
    print()

    close_terminal()
