# Universal linux updater

The Idea is one simple script to update any Linux variant. One that is smart enough to know which Linux distro it is in and update accordingly. With one simple command that is used across distros, making Linux more cohesive in the terminal.

To set it up is not hard.
Use git clone to download upg.sh

$ git clone https://gitlab.com/arrowlinux/universal-linux-updater.git

Then open your bashrc, zshrc, or the RC file for whatever shell you are using.
Add this line to it

$ alias upg="sudo /path/to/upg.sh"

Save and exit

Then open a terminal and enter

chmod +x /path/to/upg.sh

From then on, to update all you need to do is open a terminal and enter

$ upg

The first run will check if curl is installed and if it is not it will install it. 
It also might tell you that you have no internet when you do. 
This is only on the first run. It is calibrating.

Every time after the first run should be bug free. 

It will synchronize with the database, check for updates, list updates, and ask if you want to install updates. If dont enter no, it will update

Currently it works with "Arch, Manjaro, Debian, Ubuntu, Mint, and Peppermint."

As time passes more distros will be added. The Goal is for ALL distros to be added.

kudos
