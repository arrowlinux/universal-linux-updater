#!/bin/bash

TODAY="$(date)"
USR1="$(logname)"

clear ; clear
echo ""
echo -e "\e[36mStarted  \e[93m$TODAY \e[0m"

OS2="$(lsb_release -si)"
OS="$(lsb_release -sirc)"
OS1="${OS2,,}"

clr(){
  clear ; clear
}

# Error message wrapper function
ERR() {
  echo ""
  echo >&2 "$(tput bold; tput setaf 1)[-] ERROR: ${*}$(tput sgr0)"
  echo ""
  exit 86
}

# Check for root privilege function
check_root() {
  if [ "$(id -u)" -ne 0 ]; then
    ERR "You must be root"
  else
    msg 'Script has root privileges'
  fi
}

# Warning message wrapper function
warn() {
  echo >&2 "$(tput bold; tput setaf 3)[!] WARNING: ${*}$(tput sgr0)"
  echo ""
}

# Standard message wrapper function
msg() {
  echo "$(tput bold; tput setaf 2)[+] ${*}$(tput sgr0)"
}

# Standard message wrapper function
msg2() {
  echo "$(tput bold; tput setaf 6)    ${*}$(tput sgr0)"
}

msg3() {
  echo "$(tput bold; tput setaf 6)${*}$(tput sgr0)"
}

# Check for internet connection function
check_internet() {
  tool='curl'
  tool_opts='-s --connect-timeout 8'

  if ! $tool $tool_opts https://microsoft.com/ > /dev/null 2>&1; then
    warn "You do NOT have an Internet connection!"
  else
    msg 'Internet is active!'
  fi

  return $SUCCESS
}

OS_not_in_script() {
  warn "You are using ${OS1^} but this script is not set up for ${OS1^}"
  ERR "Cannot continue! Exiting..."
}

# Check if curl is installed function
check_curl() {
  if [ -f /usr/bin/curl ]; then
    msg "Curl is installed"
  else
    apt install -y curl
  fi
}

# Synchronize and update function
apt_upd() {
  if apt update -y; then
    echo ""
    clr
    apt list --upgradable
    echo ""
    return $SUCCESS
  fi

  warn "Synchronizing apt has failed. Please try manually: apt update -y"

  return $FAILURE
}

# Apt update function
apt_upg() {
  msg 'Perform full system upgrade? (apt upgrade -y) [Yn]:'
  read conf < /dev/tty
  case "$conf" in
    ''|y|Y|yes|YES|Yes|yES|yeS|YEs|YeS|yEs)
      msg 'You entered yes. This will upgrade your system'
      apt upgrade -y
      ;;
    n|N|no|NO|No|nO)
      warn 'You have chosen NOT to update your system.'
      ;;
  esac
}

# Debian-based list updates function
lst_D_upd() {
  apt list --upgradable
}

see_ubu() {
  sudo apt-get update -y
  clr
  pkgNumber=$(apt list --upgradable | wc -l)
  pkgNumber=$((pkgNumber-1))
  clr
  if [ $pkgNumber -eq 0 ]; then
    msg "Your system is up to date"
    msg2 "Nothing to do...."
    echo ""
  else
    pkgStr="packages"
    if [ $pkgNumber == 1 ]; then
      pkgStr="package"
    fi
    apt list --upgradable
    echo ""
    msg "$pkgNumber $pkgStr"
    echo ""
    apt_upg
  fi
}

see_arch() {
  clr
  pkgNumber=$(sudo pacman -Qu | wc -l)
  sudo pacman -Sy
  clr
  if [ $pkgNumber -eq 0 ]; then
    echo "System up to date"
  else
    pkgStr="package"
    if [ $pkgNumber -gt 1 ]; then
      pkgStr+="s"
    fi
    apt list --upgradable
    echo ""
    msg "$pkgNumber $pkgStr"
    echo ""
  fi
}

close_terminal() {
  warn 'Do you want to close the terminal? [Yn]:'
  read conf < /dev/tty
  case "$conf" in
    ''|y|Y)
      msg 'Goodbye! ,,,,,,'
      sleep 2
      #kill -9
      #exec "$SHELL"
      kill -9 $PPID
      ;;
    n|N)
      warn 'You have chosen NOT to close the terminal.'
      ;;
  esac
}

###############################################
################# Arch Based ##################
###############################################

# Install Pacaur for Arch-based system function
Install_Pacaur_Arch() {
  pacman -S git --noconfirm
  cd /home/$USR1/
  sudo -u $USR1 git clone https://aur.archlinux.org/auracle-git.git
  cd /home/$USR1/auracle-git/ ; sudo -u $USR1 makepkg -si --noconfirm
  cd ..
  rm -R /home/$USR1/auracle-git
  sudo -u $USR1 git clone https://aur.archlinux.org/pacaur.git
  cd /home/$USR1/pacaur/
  sudo -u $USR1 makepkg -si --noconfirm
  cd ..
  rm -R /home/$USR1/pacaur
  echo ""
  echo "Pacaur is installed."
  echo ""
  echo "Press a key to continue"
  read -rsn1
  echo "Key pressed"
  sleep 1
}

# Pacman update function
pac_upd() {
  if pacman -Sy; then
    return $SUCCESS
  fi

  warn "Synchronizing pacman has failed. Please try manually: pacman -Sy"

  return $FAILURE
}

# List Arch updates function
lst_A_upd() {
  if [ -f /usr/bin/pacaur ]; then
    see_arch
  else
    warn "Pacaur is not installed. However, it is needed in this script"
    msg "Do you want to install Pacaur now? [Yn]:"
    read Paca < /dev/tty
    case "$Paca" in
      ''|y|Y)
        msg "You entered yes. We will now install Pacaur."
        Install_Pacaur_Arch
        ;;
      n|N)
        warn 'You have chosen NOT to install Pacaur.'
        ERR 'Script cannot continue! Exiting......'
        ;;
    esac
  fi
}

# Pacman install updates function
pac_upg() {
  echo 'Perform full system upgrade? (pacman -Su) [Yn]:'
  read conf < /dev/tty
  case "$conf" in
    ''|y|Y)
      msg 'You entered yes. This will upgrade your system'
      pacman -Su --noconfirm --color always
      ;;
    n|N)
      warn 'You have chosen NOT to update your system.'
      ;;
  esac
}

solus_upd() {
  sudo eopkg up
}

solus_upg() {
  echo 'Perform full system upgrade? (eopkg upgrade) [Yn]:'
  read conf < /dev/tty
  case "$conf" in
    ''|y|Y)
      msg 'You entered yes. This will upgrade your system'
      sudo eopkg update-repo && sudo eopkg upgrade
      ;;
    n|N)
      warn 'You have chosen NOT to update your system.'
      ;;
  esac
}

# Run the correct update for the installed system function
if_then_elif() {
  # Arch based
  if [[ "${OS1}" == 'arch' ]] || [[ "${OS1}" == 'manjaro' ]]; then
    see_arch
    pac_upg

  # Debian based
  elif [[ "${OS1}" == 'ubuntu' ]] || [[ "${OS1}" == 'peppermint' ]] || [[ "${OS1}" == 'linuxmint' ]] || [[ "${OS1}" == 'debian' ]] || [[ "${OS1}" == 'kali' ]] || [[ "${OS1}" == 'raspian' ]]; then
    see_ubu
    apt_upg

  # Solus
  elif [[ "${OS1}" == 'solus' ]]; then
    solus_upd
    solus_upg
  else
    OS_not_in_script
  fi
}

list_os() {
  echo ""
  msg2 "     You are running"
  msg2 "    ${OS}"
  echo ""
}

## RUN UPDATE ##

#echo ""
TIMEFORMAT='Time taken to complete this task, %E Seconds ...'
time {
  list_os
  check_root
  check_curl
  check_internet
  echo ""
  if_then_elif
  echo ""
}
echo ""
TODAY2="$(date)"
pkgStr="packages"
TheIntro='There were'
if [ $pkgNumber == 1 ]; then
  pkgStr="package"
  TheIntro='There was'
fi
echo -e "\e[36m$pkgStr updated \e[93m$pkgNumber"
echo ""
echo -e "\e[36mStarted   \e[93m$TODAY \e[36mBy \e[94m${USR1^} \e[0m"
echo -e "\e[36mFinished  \e[93m$TODAY2\e[0m"
echo ""


close_terminal
# End of Script
#
